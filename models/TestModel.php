<?php
namespace app\models;
/**
 * Created by PhpStorm.
 * User: david
 * Date: 2018/9/25
 * Time: 17:16
 * Email:liyongsheng@meicai.cn
 */

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
    protected $table = 'content';
}