<?php

use app\models\SampleModel;
use app\models\TestModel;
/**
 * @name IndexController
 * @author david
 * @desc 默认控制器
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class IndexController extends \Yaf\Controller_Abstract {

	/** 
     * 默认动作
     * Yaf支持直接把Yaf_Request_Abstract::getParam()得到的同名参数作为Action的形参
     * 对于如下的例子, 当访问http://yourhost/Sample/index/index/index/name/david 的时候, 你就会发现不同
     */
	public function indexAction($name = "Stranger") {
		//1. fetch query
		$get = $this->getRequest()->getQuery("get", "default value");

		//2. fetch model
		$model = new SampleModel();
		/** @var TestModel $test */
        $test = new TestModel();
        $res = $test->find(1);
        var_dump($res);
		//3. assign
		$this->getView()->assign("content", $model->selectSample());
		$this->getView()->assign("name", $name);
        \Yaf\Registry::get('logger')->error("ddddd");
		//4. render by Yaf, 如果这里返回FALSE, Yaf将不会调用自动视图引擎Render模板
        return TRUE;
	}
	public function dbAction(){

        $args = array(
            'product_id'   => FILTER_SANITIZE_ENCODED,
            'component'    => array('filter'    => FILTER_VALIDATE_INT,
                'flags'     => FILTER_REQUIRE_ARRAY,
                'options'   => array('min_range' => 1, 'max_range' => 10)
            ),
            'versions'     => FILTER_SANITIZE_ENCODED,
            'doesnotexist' => FILTER_VALIDATE_INT,
            'testscalar'   => array(
                'filter' => FILTER_VALIDATE_INT,
                'flags'  => FILTER_REQUIRE_SCALAR,
            ),
            'testarray'    => array(
                'filter' => FILTER_VALIDATE_INT,
                'flags'  => FILTER_REQUIRE_ARRAY,
            )

        );

        $myinputs = filter_input_array(INPUT_POST, $args);
	    echo "dddd";
	    return true;
    }
}
