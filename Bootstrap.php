<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use app\plugins\SamplePlugin;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Yaf\Registry;

/**
 * @name Bootstrap
 * @author david
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:Yaf_Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends \Yaf\Bootstrap_Abstract {

    public function _initLoader() {
        /** @var \Composer\Autoload\ClassLoader $loader */
        $loader = require APP_PATH . '/vendor/autoload.php';
        $loader->setPsr4("app\\controllers\\", [APP_PATH."/controllers"]);
        $loader->setPsr4("app\\models\\", [APP_PATH."/models"]);
        $loader->setPsr4("app\\plugins\\", [APP_PATH."/plugins"]);
        $loader->setPsr4("app\\library\\", [APP_PATH."/library"]);
    }
    public function _initLogger(){
        $config = \Yaf\Application::app()->getConfig();
        $logger = new Logger('app');
        $levelMap = Logger::getLevels();
        if(isset($config['log']['level']) && isset($levelMap[$config['log']['level']])){
            $level = $levelMap[$levelMap[$config['log']['level']]];
        }else{
            $level = Logger::WARNING;
        }
        $logger->pushHandler(new RotatingFileHandler($config['log']['path'], 0, $level));
        $logger->info('_initLogger');
        \Yaf\Registry::set('logger', $logger);
    }
    public function _initDatabase()
    {
        /** @var \Yaf\Config\Ini */
        $config = \Yaf\Application::app()->getConfig()->toArray();

        $capsule = new Capsule;
        $capsule->addConnection($config['db']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
    public function _initPlugin(\Yaf\Dispatcher $dispatcher) {
		//注册一个插件
		$objSamplePlugin = new SamplePlugin();
		$dispatcher->registerPlugin($objSamplePlugin);
	}
    /**
     * 注册validator工厂方法
     */
    public function _initValidator(){
        Registry::set('validator', function (): \Illuminate\Validation\Factory {
            $validator = Registry::get('_validator');
            if(empty($validator)) {
                $translationPath = \Yaf\Application::app()->getAppDirectory() . '/application/lang';
                $translationLocale = 'en';
                $translationFileLoader = new \Illuminate\Translation\FileLoader(new \Illuminate\Filesystem\Filesystem, $translationPath);
                $translator = new \Illuminate\Translation\Translator($translationFileLoader, $translationLocale);
                $validator = new \Illuminate\Validation\Factory($translator);
                Registry::set('_validator', $validator);
            }
            return $validator;
        });
        Registry::get('logger')->info('_initValidator');
    }

    /**
     * 注册获取predis client 实例的方法 保证redis单例
     */
    public function _initRedis(){
        Registry::set('redis', function():\Predis\Client{
            $predis = Registry::get('_predis');
            if(empty($predis)){
                $config = \Yaf\Application::app()->getConfig();
                $predis = new \Predis\Client([
                    'scheme' => 'tcp',
                    'host'   => $config['redis']['host']??'127.0.0.1',
                    'port'   => $config['redis']['port']??'6379',
                ]);
                Registry::set('_predis', $predis);
            }
            return $predis;
        });
        Registry::get('logger')->info('_initValidator');
    }
	public function _initRoute(\Yaf\Dispatcher $dispatcher) {
		//在这里注册自己的路由协议,默认使用简单路由
	}
	
	public function _initView(\Yaf\Dispatcher $dispatcher) {
		//在这里注册自己的view控制器，例如smarty,firekylin
	}
}